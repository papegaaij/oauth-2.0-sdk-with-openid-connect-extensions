Nimbus OAuth 2.0 SDK with OpenID Connect extensions

Copyright (c) Connect2id Ltd., 2012 - 2021


README

This open source SDK is your starting point for developing OAuth 2.0 and OpenID
Connect based applications in Java.

This SDK version implements the following standards and drafts:

	* The OAuth 2.0 Authorization Framework (RFC 6749)

	* The OAuth 2.0 Authorization Framework: Bearer Token Usage (RFC 6750)

	* OAuth 2.0 Token Introspection (RFC 7662)

	* OAuth 2.0 Token Revocation (RFC 7009)

	* OAuth 2.0 Authorization Server Metadata (RFC 8414)

	* OAuth 2.0 Dynamic Client Registration Protocol (RFC 7591)

	* OAuth 2.0 Dynamic Client Registration Management Protocol (RFC 7592)

	* Assertion Framework for OAuth 2.0 Client Authentication and Authorization
	  Grants (RFC 7521)

	* JSON Web Token (JWT) Profile for OAuth 2.0 Client Authentication and
      Authorization Grants (RFC 7523)

    * SAML 2.0 Profile for OAuth 2.0 Client Authentication and Authorization
      Grants (RFC 7522)

    * Proof Key for Code Exchange by OAuth Public Clients (RFC 7636)

    * Authentication Method Reference Values (RFC 8176)

    * OAuth 2.0 Authorization Server Metadata (RFC 8414)

    * OAuth 2.0 Mutual TLS Client Authentication and Certificate Bound Access
      Tokens (RFC 8705)

    * OAuth 2.0 Demonstrating Proof-of-Possession at the Application Layer
      (DPoP) (draft-ietf-oauth-dpop-02)

    * Resource Indicators for OAuth 2.0 (RFC 8707)

    * OAuth 2.0 Device Authorization Grant (RFC 8628)

    * OAuth 2.0 Incremental Authorization
      (draft-ietf-oauth-incremental-authz-04)

    * The OAuth 2.0 Authorization Framework: JWT Secured Authorization Request
      (JAR) (draft-ietf-oauth-jwsreq-29)

    * OAuth 2.0 Pushed Authorization Requests (draft-ietf-oauth-par-02)

    * OAuth 2.0 Authorization Server Issuer Identifier in Authorization
      Response (draft-ietf-oauth-iss-auth-resp-00)

	* OpenID Connect Core 1.0 (2014-02-25)

	* OpenID Connect Core Unmet Authentication Requirements 1.0 (2019-05-08)

	* OpenID Connect Discovery 1.0 (2014-02-25)

	* OpenID Connect Dynamic Registration 1.0 (2014-02-25)

	* OpenID Connect Session Management 1.0 (2017-01-25)

	* OpenID Connect Front-Channel Logout 1.0 (2017-01-25)

	* OpenID Connect Back-Channel Logout 1.0 (2017-01-25)

	* OpenID Connect Extended Authentication Profile (EAP) ACR Values 1.0 -
	  draft 00

	* OpenID Connect for Identity Assurance 1.0 - draft 11

	* OpenID Connect Federation 1.0 - draft 12

	* Initiating User Registration via OpenID Connect (draft 03)

	* OAuth 2.0 Multiple Response Type Encoding Practices 1.0 (2014-02-25)

    * Financial Services – Financial API - Part 1: Read Only API Security
      Profile (2018-10-17)

    * Financial Services – Financial API - Part 2: Read and Write API Security
      Profile (2018-10-17)

    * Financial-grade API: JWT Secured Authorization Response Mode for OAuth
      2.0 (JARM) (2018-10-17)

    * OpenID Connect Client Initiated Backchannel Authentication (CIBA) Flow -
      Core 1.0 (draft 03)


This SDK is provided under the terms of the Apache 2.0 license.

Questions or comments? Email support@connect2id.com